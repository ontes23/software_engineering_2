# Gui Testing Documentation template

Authors: Team B

Date: 18/10/2019

Version: 1

# Contents

- [Scenarios](#scenarios)

# Scenarios


| Scenario ID: SC1 |                                                |
| ---------------- | ---------------------------------------------- |
| Description      | The citizen wants to get an account ticket     |
| Precondition     | Ticket office must be open                     |
| Postcondition    | The ticket is printed                          |
| Step#            | Step description                               |
| 1                | Citizen clicks the account ticket button       |
| 2                | The notification window appears                |
| 3                | Citizen clicks ok                              |
| 4                | The citizen see the account ticket which includes ticket number and time |

| Scenario ID: SC2 |                                                        |
| ---------------- | ------------------------------------------------------ |
| Description      | The citizen wants to get an package ticket             |
| Precondition     | Ticket office must be open 
| Postcondition    | The ticket is printed                                  |
| Step#            | Step description                                       |
| 1                | Citizen clicks the package ticket button               |
| 2                | The notification window appears                        |
| 3                | Citizen clicks ok                                      |
| 4                | The citizen see the package ticket which includes ticket number and time |





