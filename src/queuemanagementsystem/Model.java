/*
 * To change this license header, choose License Headers in 
Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuemanagementsystem;

import java.util.ArrayList;
import java.util.Date;
/*;
 *
 * @author aydat
 */
public class Model {
	ArrayList<Ticket> Tqueue;
	private int counter_package;
	private int counter_account;
	
	public Model() {
		 this.counter_account=1;
		 this.counter_package=1;
		 Tqueue= new ArrayList<Ticket>();
	}
	
	public int getCounter_package() {
		return counter_package;
	}



	public int getCounter_account() {
		return counter_account;
	}

	public Ticket new_ticket(String type) {
		Date d = new Date(); 
		Ticket t;
		if (type.equals("package")) {
			
			 t = new Ticket(type, counter_package, d);
			 if(counter_package!=999)
				 counter_package ++;
			 else
				 counter_package=1;
		}else {
			 t = new Ticket(type, counter_account, d);
			 if(counter_account!=999)
					counter_account ++;
					 else
						 counter_account=1;
	}
    
		Tqueue.add(t);
		return t;

	}
	
	
}
