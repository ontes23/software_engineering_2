/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuemanagementsystem;


import java.util.Date;

/**
 *
 * @author aydat
 */
public class Ticket {
	
    private String type;
    private int number;
    private Date date;

    public Ticket(String type, int number, Date date) {
		this.type = type;
		this.number = number;
		this.date = date;
		
	}

	public String getType() {
        return type;
    }
     
      public void setType(String type) {
        this.type = type;
    }

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String GetCode() {
		String Code= "";
		if (type.equals("package")) {
			Code += "P";
			
		}else {
			Code += "A";
	}
		Code += String.format("%03d",number);
		return Code;
	}


       
}
