/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuemanagementsystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.sql.Timestamp;

/**
 *
 * @author aydat
 */
public class Controller {
    
    public Model m;
    public View v;
    
     public Controller(Model m, View w) {
        this.m = m;
        this.v = w;
        this.v.listenerAccountButton(new accountButtonListener());
        this.v.listenerPackageButton(new packageButtonListener());
        
      }
    
      class accountButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(null, "Ticket is printing...");
            Ticket t = m.new_ticket("account");
               v.getTicketNumber().setText(t.GetCode());
               v.getTicketPanel().setVisible(true);
               v.getTimestampLabel().setText(t.getDate().toLocaleString());   
        }
        
    }
      
      class packageButtonListener implements ActionListener {
        @SuppressWarnings("deprecation")
		@Override
        public void actionPerformed(ActionEvent e) {
         JOptionPane.showMessageDialog(null, "Ticket is printing...");
         Ticket t = m.new_ticket("package");
            v.getTicketNumber().setText(t.GetCode());
            v.getTicketPanel().setVisible(true);
            v.getTimestampLabel().setText(t.getDate().toLocaleString());   
        }
        
    } 
 
  
}
