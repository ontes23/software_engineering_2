package Junit_test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import queuemanagementsystem.Model;
import queuemanagementsystem.Ticket;

class Test {

	
	@org.junit.jupiter.api.Test
	void test_model_newTicket_account() {
		Model m = new Model();
		Ticket t = m.new_ticket("account");
		assertTrue(m.getCounter_account()==2);
		Ticket t2 = m.new_ticket("account");
		assertTrue(m.getCounter_account()==3);
	}
	
	@org.junit.jupiter.api.Test
	void test_model_newTicket_package() {
		Model m = new Model();
		Ticket t = m.new_ticket("package");
		assertTrue(m.getCounter_package()==2);
		Ticket t2 = m.new_ticket("package");
		assertTrue(m.getCounter_package()==3);
	}
	
	
	
	@org.junit.jupiter.api.Test
	void test_ticket_creation_account() {
		Date d = new Date();
		Ticket t = new Ticket("account",4,d);
		assertTrue(t.GetCode().equals("A004"));
		assertTrue(t.getDate().equals(d));
		assertTrue(t.getNumber()==4);
	}
	
	@org.junit.jupiter.api.Test
	void test_ticket_creation_package() {
		Date d = new Date();
		Ticket t = new Ticket("package",4,d);
		assertTrue(t.GetCode().equals("P004"));
		assertTrue(t.getDate().equals(d));
		assertTrue(t.getNumber()==4);
	}
	
	@org.junit.jupiter.api.Test
	void test_ticket_getters_setters() {
		Date d = new Date();
		Ticket t = new Ticket("package",4,d);
		assertTrue(t.getType().equals("package"));
		t.setType("account");
		assertTrue(t.getType().equals("account"));
		t.setNumber(89);
		assertTrue(t.getNumber()==89);
		Date d2 = new Date();
		t.setDate(d2);
		assertTrue(t.getDate().equals(d2)); 
		
	}
	
	@org.junit.jupiter.api.Test
	void test_model_creation() {
		Model m = new Model();
		assertTrue(m.getCounter_account()==1);
		assertTrue(m.getCounter_package()==1);
	}
	
	
	@org.junit.jupiter.api.Test
	void test_model_newTicket_reset_counter_account() {
		Model m = new Model();
		Ticket t;
		for(int i=1 ; i<999 ; i++) {
			 t= m.new_ticket("account");
		}
		assertTrue(m.getCounter_account()==999);
		t= m.new_ticket("account");
		assertTrue(m.getCounter_account()==1);
		t= m.new_ticket("account");
		assertTrue(m.getCounter_account()==2);
	}
	
	@org.junit.jupiter.api.Test
	void test_model_newTicket_reset_counter_package() {
		Model m = new Model();
		Ticket t;
		for(int i=1 ; i<999 ; i++) {
			 t= m.new_ticket("package");
		}
		assertTrue(m.getCounter_package()==999);
		t= m.new_ticket("package");
		assertTrue(m.getCounter_package()==1);
		t= m.new_ticket("package");
		assertTrue(m.getCounter_package()==2);
	}


	
}
